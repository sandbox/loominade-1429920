Neostandard
===========

Dieses Theme soll so simpel sein wie Drupal ohne Theme nur ohne:

* Unnötige Wrapper
* Unnötige Klassen

Entwickler von sub-themes können diese dann per opt-in erlauben, durch 
das anlegen einer Konstantenten oder so.

Außerdem wird bestimmtes Default CSS durch simpleres CSS ersetzt:

* Zum Beispiel die Menüs. Hier wird auf die ganzen collapsed und 
  expendet indikatoren verzichtet.
* Die Tabs werden vereinfacht

Entfernte Klassen
-----------------

* body-tag
* theme_field
* theme_menu_link
* theme_item_list

Entfernte Wrapper
-----------------

* wrapper um field items
* wrapper um item lists
* content-rapper in blocks
