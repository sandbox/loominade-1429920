<?php

// see neostandard_menu_tree
define('NEOSTANDARD_MENU_TREE_CLASS', true);

// see neostandard_field
 define('NEOSTANDARD_FIELD_ITEMS_WRAPPER', false); 
 define('NEOSTANDARD_FIELD_ITEM_WRAPPER', false);

 define('NEOSTANDARD_BLOCK_CONTENT_WRAPPER', false);


$GLOBALS['allowed_classes'] = array(
    'html_body' => array(
    	'page-admin',
      //'front',
      //'logged-in',
      //'one-sidebar', 
      //'sidebar-second', 
      //'page-node',
	  ),
	  'node' => array(
	  	'node',
	  	'node-unpublished',
		),
		  'field' => array(
      'field',
	  ),
    'menu_link' => array(
      'active-trail',
    ),
    'field_item' => array(
      //'odd',
      //'even',
    ),
    'block' => array(
      'block',  
    ),
    'table' => array(
      'sticky-enabled',
    ),
);

function neostandard_remove_unnecessary_classes(&$classes, $context) {
	global $allowed_classes;
	if(is_string($classes)) {
		$classes = explode(' ', $classes);
		$was_string = TRUE;
	}
	if(isset($allowed_classes[$context])) {
		foreach (element_children($classes) as $key) {
	    if(!in_array($classes[$key], $allowed_classes[$context])) {
	    	unset($classes[$key]);
	    }
	  }
	}
	if(isset($was_string) && $was_string) {
		$classes = implode($classes);
	}
}

include('theme/field.inc');
include('theme/menu.inc');
include('theme/theme.inc');

include('template_preprocess/block.inc');
include('template_preprocess/node.inc');
include('template_preprocess/theme.inc');

if(module_exists('views')) {
  include('template_preprocess/views.inc');
}






