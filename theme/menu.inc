<?php

function neostandard_menu_tree($variables) {
  if(!NEOSTANDARD_MENU_TREE_CLASS){
  	return '<ul>' . $variables['tree'] . '</ul>';
  } else {
    return '<ul class="menu">' . $variables['tree'] . '</ul>';
  }
}

function neostandard_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if(isset($element['#attributes']['class'])) {
    neostandard_remove_unnecessary_classes($element['#attributes']['class'], 'menu_link');
  }

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
