<?php

function neostandard_field($variables) {
  $output = '';
  neostandard_remove_unnecessary_classes($variables['classes'], 'field');

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $field_items = '';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    neostandard_remove_unnecessary_classes($classes, 'field_item');
    if(NEOSTANDARD_FIELD_ITEM_WRAPPER) {
      $field_items .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
    } else {
      $field_items .= drupal_render($item);
    }
  }
  if(NEOSTANDARD_FIELD_ITEMS_WRAPPER) {
    $output .= '<div class="field-items"' . $variables['content_attributes'] . '>' . $field_items . '</div>';
  } else {
    $output .= $field_items;
  }


  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}
