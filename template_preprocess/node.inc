<?php 

function neostandard_preprocess_node(&$variables) {
   neostandard_remove_unnecessary_classes($variables['classes_array'], 'node');
}
