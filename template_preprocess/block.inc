<?php

function neostandard_preprocess_block(&$variables) {
  neostandard_remove_unnecessary_classes($variables['classes_array'], 'block');
  if(NEOSTANDARD_BLOCK_CONTENT_WRAPPER) {
    $variables['content_wrapper'] = TRUE;
  } else {
    $variables['content_wrapper'] = FALSE;
  }
}
