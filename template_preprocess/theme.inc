<?php

function neostandard_preprocess_html(&$variables) {
  neostandard_remove_unnecessary_classes($variables['classes_array'], 'html_body');
}
